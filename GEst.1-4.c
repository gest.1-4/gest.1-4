//GUIA ESTRUCTURAS EJERCICIO 1
//Declare una estructura para almacenar datos de estudiantes (DNI y dos notas correspondientes a los dos cuatrimestres del anio).
//Haga un programa que permita ingresar los datos de 5 estudiantes en un vector de estas estructuras.
//Luego de ingresar los datos se deben mostrar los DNI de cada estudiante y el promedio que tiene en sus examenes.

#include <stdio.h>

struct estudiantes

{
    int dni;                                                  
    float nota1;
    float nota2;
};

//Declaro la estructura estudiantes y los datos que incluira.

int main()
{
    int i = 0;
    float promedio;   
    struct estudiantes estudiante[5];

//Creo un vector de estructuras para asignar una posicion por cada estudiante.

    for(i=0; i<5; i++)
    {
        printf("Ingrese el DNI del estudiante N%d: ", i+1);
        scanf("%d", &estudiante[i].dni);
        printf("Ingrese la 1er nota del estudiante N%d: ", i+1); 
        scanf("%f", &estudiante[i].nota1);                         
        printf("Ingrese la 2da nota del estudiante N%d: ", i+1);
        scanf("%f", &estudiante[i].nota2);
    }

    for(i=0; i<5; i++)
    {
        promedio = (estudiante[i].nota1 + estudiante[i].nota2)/2;
        printf("DNI: %d => Promedio: %.2f\n", estudiante[i].dni, promedio);
    }

//Muestro en pantalla el DNI de cada estudiante acompaņado del promedio que tiene en su examenes.

}